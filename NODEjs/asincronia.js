const fs = require('fs')

const filename = process.argv[2]

if (!filename) {
    throw Error('Algo salio mal')
}

fs.watch(filename, ()=> {
    console.log(`Modificado el archivo ${filename}`)

})

console.log(`Comprobaando estatus del archivo ${filename}`)