var mongoose = require('mongoose')

var userSchema = new mongoose.Schema({
    nombre:String,
    apellido: String,
    edad:Number
})

var Usuario = mongoose.model('Usuario',userSchema)


module.exports = Usuario