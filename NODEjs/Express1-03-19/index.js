const express = require ('express');
var bodyparser = require('body-parser');
var mongoose = require("mongoose");
var Usuario = require ("./Usuario");

var app = express();
app.use(bodyparser.json());


app.use(express.static(__dirname + '/public'));
const DBURL= 'mongodb+srv://admin:upgrade123hub@cluster0-pa9tf.mongodb.net/test?retryWrites=true'
mongoose.connect(DBURL);

var db = mongoose.connection;

db.on('error', console.error.bind(console,'connection error'));

db.once('open', () => {
    console.log('Conectados');
})


app.get('/user', (req,res) => {
    // res.send(`${user.nombre} y ${user.apellido}`);
    // var user =new Usuario
    Usuario.find({},(err,users)=> {
        if (err) { console.log(err)}
        res.send(users)
    })
});

app.post('/user', (req,res) => {
    // user.nombre= req.body.nombre;
    // user.apellido = req.body.apellido;
    // res.send(`se ha guardado Nombre: ${user.nombre} y Apellido: ${user.apellido}`);
    var user = new Usuario({
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        edad: req.body.edad
    })
    user.save( err => {
        if (err) {return handleError(err)}
        else { return res.send('Guardado sin problemas')}
    })

});

app.delete('/user', (req,res) => {
    // user = {};
    // res.send("Borraste todo!");

    Usuario.deleteOne({_id: req.body._id}, (err) => {
        handleError(err)
    })
    res.send('Eliminado el registro')
});

app.put('/user', (req,res) => {
    // if(req.body.nombre) {
    //     user.nombre = req.body.nombre;
    // }
    // if(req.body.apellido){
    //     user.apellido = req.body.apellido;
    // }
    // res.send(`se ha guardado Nombre: ${user.nombre} y Apellido: ${user.apellido}`);
});

app.listen(3000);