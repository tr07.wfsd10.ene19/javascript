const express = require ('express');
var bodyparser = require('body-parser');

var app = express();
app.use(bodyparser.json());
var user = {};
app.use(express.static(__dirname + '/public'));

app.get('/user', (req,res) => {
    res.send(`${user.nombre} y ${user.apellido}`);
});

app.post('/user', (req,res) => {
    user.nombre= req.body.nombre;
    user.apellido = req.body.apellido;
    res.send(`se ha guardado Nombre: ${user.nombre} y Apellido: ${user.apellido}`);
});

app.delete('/user', (req,res) => {
    user = {};
    res.send("Borraste todo!");
});

app.put('/user', (req,res) => {
    if(req.body.nombre) {
        user.nombre = req.body.nombre;
    }
    if(req.body.apellido){
        user.apellido = req.body.apellido;
    }
    res.send(`se ha guardado Nombre: ${user.nombre} y Apellido: ${user.apellido}`);
});

app.listen(3000);