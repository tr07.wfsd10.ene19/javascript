import React, { Component } from "react"


export default class Movies extends Component{

    data = [
        {
          "id":"1",
          "nombre": "Casablanca",
          "bio": "A cynical nightclub owner protects an old flame and her husband from Nazis in Morocco.",
          "img": "assets/img/casablanca.jpg",
          "aparicion": "1942"
        },
        {
          "id":"2",
          "nombre": "Blade Runner",
          "bio": "A blade runner must pursue and terminate four replicants who stole a ship in space, and have returned to Earth to find their creator.",
          "img": "assets/img/blade_runner.jpg",
          "aparicion": "1982"
        },
        {
          "id":"3",
          "nombre": "Golpe en la pequeña China",
          "bio": "A rough-and-tumble trucker helps rescue his friend's fiance from an ancient sorcerer in a supernatural battle beneath Chinatown.",
          "img": "assets/img/golpe_pequena_china.jpg",
          "aparicion": "1986"
        },
        {
          "id":"4",
          "nombre": "Zoolander",
          "bio": "At the end of his career, a clueless fashion model is brainwashed to kill the Prime Minister of Malaysia.",
          "img": "assets/img/zoolander.jpg",
          "aparicion": "2001"
        },
        {
          "id":"5",
          "nombre": "Pesadilla en Elm Street",
          "bio": "The monstrous spirit of a slain janitor seeks revenge by invading the dreams of teenagers whose parents were responsible for his untimely death.",
          "img": "assets/img/pesadilla_elm_street.jpg",
          "aparicion": "1986"
        }
      ]
      constructor(props){
        super(props)
        this.state = {
            number: 0
        }
        this.cambiar = this.cambiar.bind(this)
      }
      cambiar(e) {
          if(this.state.number == this.data.length -1){
              this.setState({
                  number:0
              })
          }else {

              this.setState({
                  number: this.state.number + 1
              })
          }
      }
    render() {
        return(
            [
                <h1>{this.data[this.state.number].nombre}</h1>,
                <img src={this.data[this.state.number].img} alt="foto de la pelicula"/>,
                <button onClick={this.cambiar}>Siguiente</button>
            ]
        )
    }

}