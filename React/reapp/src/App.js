import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import * as data from './peliculas.json';


class App extends Component {
  render() {
    return (
      <div className="App">
        {data}
      </div>
    );
  }
}

export default App;
