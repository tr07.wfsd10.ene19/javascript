import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { MoviesService, Peliculas } from 'src/services/movies.service';
@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit {
  
  movie:any;
  
  constructor(private activatedRoute:ActivatedRoute, private _movieService:MoviesService) {
    this.activatedRoute.params.subscribe(p => {    
      this._movieService.getMovieById(p['id']).subscribe(d => {
        this.movie = d;
      });
      console.log(p['id']);
      console.log(this.movie);
    })
   }

  ngOnInit() {
    
  }

}
