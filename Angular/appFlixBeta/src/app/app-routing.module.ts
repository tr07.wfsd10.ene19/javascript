import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {HomeComponent} from './home/home.component';
import { AboutComponent } from './about/about.component';
import { PeliculasComponent } from './peliculas/peliculas.component';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';
import { BuscadorComponent } from './buscador/buscador.component';

const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'about',component:AboutComponent},
  {path: 'details/:id',component: MovieDetailComponent},
  {path:'peliculas',component:PeliculasComponent},
  {path:'buscador/:title', component: BuscadorComponent},
  {path:'**',pathMatch:'full', redirectTo:'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
