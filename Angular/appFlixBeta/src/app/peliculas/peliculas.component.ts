import { Component, OnInit } from '@angular/core';
import { MoviesService, Peliculas} from '../../services/movies.service';

@Component({
  selector: 'app-peliculas',
  templateUrl: './peliculas.component.html',
  styleUrls: ['./peliculas.component.css']
})
export class PeliculasComponent implements OnInit {
  peliculas:Peliculas[] = [];
  constructor(private _movieService:MoviesService) { }
  ngOnInit() {
    this._movieService.getData().subscribe(lista => {
      this.peliculas = lista['results'];
    });
  }
  
}
