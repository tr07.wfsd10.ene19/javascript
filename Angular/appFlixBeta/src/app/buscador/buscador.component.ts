import { Component, OnInit } from '@angular/core';
import { MoviesService, Peliculas } from 'src/services/movies.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css']
})
export class BuscadorComponent implements OnInit {
  mostrar:any[];
  constructor(private _movieService:MoviesService, private activatedRoute:ActivatedRoute) {
    this.activatedRoute.params.subscribe(p => {    
      this._movieService.getMovieByTitle(p['title']).subscribe(d => {
        this.mostrar = d['results'];
      });
    })
   }

  ngOnInit() {
  }

}
