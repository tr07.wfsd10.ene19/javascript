import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pruebaPipe'
})
export class PruebaPipePipe implements PipeTransform {
  urlImages = 'https://image.tmdb.org/t/p/w500/';
  transform(value: any, args?: any): any {
    return this.urlImages + value;
  }

}
