import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.css']
})
export class MovieCardComponent implements OnInit {
  
  @Input() p:any;
  visible=false;
  constructor(private router:Router) { }

  ngOnInit() {
  }
  goTo(id:number) {
    this.router.navigate(['/details',id]);
  }
}
