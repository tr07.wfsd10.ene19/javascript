import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class MoviesService {
   
  urlApi = 'https://api.themoviedb.org/3';
  apiKey = '4d3e109968bdbba1599acf5ff41347dd';
  private movies:any[]= [];
   url = "http://localhost:4200/assets/peliculas.json";
  constructor(private http:HttpClient) {
    
   }

  getData() {
    const discoverMovies = `${this.urlApi}/discover/movie?sort_by=popularity.desc&api_key=${this.apiKey}`;
    return this.http.get<any[]>(discoverMovies);
  }

  getMovieById(id: string):any{
    const movieId = `${this.urlApi}/movie/${id}?api_key=${this.apiKey}`;
    return this.http.get<any[]>(movieId);
  }

  getMovieByTitle(title:string):any{
    // var encontradas:any[]=[];
    // for(let x of this.movies) {
    //   let min = x.nombre.toLowerCase();
    //   if(min.indexOf(title)!= -1) {
    //     encontradas.push(x);
    //   }
    // }
    // return encontradas;
    const urlSearch = `${this.urlApi}/search/movie?query=${title}&api_key=${this.apiKey}`;
    return this.http.get<any[]>(urlSearch);
  }
}
export interface Peliculas{
  id:string;
  nombre:string,
  bio:string,
  img:string,
  aparicion: string
}
