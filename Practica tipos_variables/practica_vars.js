// Arrays, cadenas, obejtos

var txt = "En un agujero en el suelo, vivía un hobbit. No un agujero húmedo, sucio, repugnante, con restos de gusanos y olor a fango, ni tampoco un agujero seco, desnudo y arenoso, sin nada en que sentarse o que comer: era un agujero-hobbit, y eso significa comodidad."
var sln = txt.length
console.log(sln)

var txtArray = txt.split(",") //convierte la cadena en un arreglo
console.log(txtArray)

var pos = txt.indexOf("hobbit") //cuenta desde el principio de la cadena
console.log(pos)

var pos1 = txt.lastIndexOf("hobbit") //cuenta desde el principio de la cadena pero inicia la busqueda desde la parte final de la cadena
console.log(pos1)

var pos2 = txt.indexOf("Pepe") //devuelve -1 si no encuentra nada
console.log(pos2)

var pos3 = txt.search("hobbit") // parecido al indexOf pero permite le uso de expresiones regulares ej: regex
console.log(pos3)

var pos4 = txt.slice(7,13) //corta un pedazo de la cadena y devuelve lo que contiene entre los argumentos que recibe
console.log(pos4)

var pos5 = txt.substr("hobbit") //parecido a slice pero devuelve un pedazo de la cadena desde el primer argumento y los siguientes caracteres dados por el segundo valor
console.log(pos5)

var pos6 = txt.replace("hobbit","duende") //reemplaza la primera coincidencia
console.log(pos6)

var pos7 = txt.replace(/hobbit/g,"duende") //reemplaza todas las coincidencias por el uso de expresiones regulares
console.log(pos7)

var pos8 = txt.toUpperCase() //todo a mayusculas
console.log(pos8)

var pos9 = txt.toLowerCase() //todo a minusculas
console.log(pos9)

var texto = "     hola         " 
console.log(texto.trim())  //elimina espacios a la izquierda y derecha de la cadena

var peliculas = ["El señor de los anillos", "Zoolander", "Blade Runner", "La historia interminable"];
console.log(peliculas.length)
console.log(peliculas[1])

var newStr = peliculas.toString() //convierte el array en cadena
console.log(newStr)

var newStr = peliculas.join() //convierte el array en cadena, se puede pasar por parametros el separador
console.log(newStr)

var res = peliculas.pop() //elimina el ultimo elemento del array
console.log(res)
console.log(peliculas)

var algo = peliculas.push(res) //agrega al array un elemento pero siempre al final del mismo
console.log(res)
console.log(peliculas)

var res = peliculas.shift() //elimina el primer elemento del array
console.log(res)
console.log(peliculas)

var res = peliculas.unshift() //agrega al principio del array
console.log(res)
console.log(peliculas)

var res = peliculas.splice(2,0,"Pesadilla en Elm Street","Terminator") // inserta en el array en la posicion a partir del primer valor pasado y el segundo valor indica los campos que se sustituiran
console.log(peliculas)

var pelisMalas = ["Serpientes en el Avion", "Sharknado"] //concatenar arrays
var pelis = peliculas.concat(pelisMalas)
console.log(pelis)
console.log(peliculas)

var res = peliculas.slice(2) //permite copiar el valor de una posicion de un array
console.log(res)
console.log(peliculas)

var res = peliculas.slice(2,3) //crea un nuevo array con los datos extraidos del array original
console.log(res)
console.log(peliculas)

var oPeliculas = peliculas.sort() //ordena alfabeticamente el array, modifica el array original
console.log(oPeliculas)

var oPeliculas = peliculas.reverse() //escribe el array de atras hacia adelante
console.log(oPeliculas)

var numeros =  [10,30,22,1,56] //funcion para ordenar numeros de mayor a menor con numeros, y b - a ordenaria de mayor a menor
numeros.sort(function(a,b) {return a-b})
console.log(numeros)

console.log(Math.max.apply(null,numeros)) // funcion para encontrar el maximo de los numeros de un arreglo, si se quita el apply se pueden pasar los numeros directamente

console.log(peliculas.indexOf("Zoolander")) //devuelve posicion del elemento buscado

