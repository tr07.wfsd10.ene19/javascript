var parrafos, parrafo
var btn, btnMayus, btnName,btnReorder
var flag, flag2 = false
window.onload = iniciar()

function iniciar() {
    parrafos = document.querySelectorAll("p")
    
    btn = document.createElement("button")
    var text = document.createTextNode("Clickeame")
    btn.appendChild(text)

    btnMayus = document.createElement("button")
    var text2 = document.createTextNode("MAYUSCULA")
    btnMayus.appendChild(text2)

    btnName = document.createElement("button")
    var text3 = document.createTextNode("Cambiar nombre")
    btnName.appendChild(text3)

    btnReorder = document.createElement("button")
    var text4 = document.createTextNode("Cambiar orden")
    btnReorder.appendChild(text4)

    document.body.appendChild(btn)
    document.body.appendChild(btnMayus)
    document.body.appendChild(btnName)
    document.body.appendChild(btnReorder)

    btnMayus.onclick = function() {
        if (!flag2 ) {
            parrafos[2].style.textTransform ="uppercase"
            flag2 =true
            btnMayus.innerHTML = "minuscula"
        }else{
            parrafos[2].style.textTransform ="initial"
            flag2 = false
            btnMayus.innerHTML = "MAYUSCULA"
        }
    }
    
    btn.onclick = function (){
        if(flag != true){
            parrafos[1].style.color = "green"
            flag = true
        }
        else {
            parrafos[1].style.color = "black"
            flag =false
        }
    }
    btnName.onclick = function (){
        changeName("Luis")
    }
    btnReorder.onclick = function (){
        reorder()
    }
    function changeName(nombre){
        parrafo = parrafos[0].innerHTML.split(" ")
        for (i=0;i <parrafo.length;i++){
            if (parrafo[i] == "Ismael."){
                parrafo[i] = nombre
            }
        }
        var parrafo2 = parrafo.join(" ")
        parrafos[0].innerHTML = parrafo2
    }

    function reorder() {
        for (i=parrafos.length-1; i >= 0; i--){
            var newParrafos= parrafos[i].cloneNode(true)
            parrafos[i].remove()
            document.body.appendChild(newParrafos)
        }
        
    }
}
