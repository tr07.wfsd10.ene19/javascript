//lectura del DOM

//botones
var btnGuardar = document.querySelector("#btnGuardar")
var btn = document.querySelector("button")

//template
var template = document.querySelector("#tabla")
var fila = document.querySelector("#tablaRow")
var colId = document.querySelector("#idPelicula")
var colTitulo = document.querySelector("#tituloPelicula")
var colGenero = document.querySelector("#generoPelicula")
var colAnno = document.querySelector("#annoPelicula")
var colBotones = document.querySelector("#botonesTabla")

//form
var nombrePeli = document.querySelector("#nombrePeli")
var generoPeli = document.querySelector("#generopeli")
var annoPeli = document.querySelector("#annopeli")


class Panel {
  constructor() {
    this.listado = [{
        id: 1,
        titulo: 'Batman Begins',
        genero: 'Accion',
        anno: 2009
      },
      {
        id: 2,
        titulo: 'Batman vs Superman',
        genero: 'Accion',
        anno: 2010
      }
    ]
  }

  get getPeliculas() {
    return this.listado

  }

  get lastId() {
    return this.listado.length
  }
  setPeliculas(pelicula) {
    if (pelicula instanceof Pelicula) {
      this.listado.push(pelicula)
    }
  }

  dibujarTemplate() {
    var tabla = document.querySelector("#tabla")
    var newFila = tabla.content.querySelector("#tablaRow")
    this.listado.forEach((e) => {
      var a = newFila.cloneNode(true)
      a.querySelector("#idPelicula").innerHTML = e.id
      a.querySelector("#tituloPelicula").innerHTML = e.titulo
      a.querySelector("#generoPelicula").innerHTML = e.genero
      a.querySelector("#annoPelicula").innerHTML = e.anno
      a.querySelector("#btnE").id = "btnE"+e.id
      a.querySelector("#btnB").id = "btnB"+e.id
      document.querySelector("tbody").appendChild(a)
    })
  }

  llenarInputs(pelicula) { //poner en el boton editar
    if(pelicula) {
      nombrePeli.value = pelicula.titulo
      generoPeli.value = pelicula.genero
      annoPeli.value = annoPeli
    }
  }

  guardarBd(pelicula) {
    if(pelicula instanceof Pelicula){
      if(pelicula.id){
        this.listado.forEach((e,i)=> {
          if(e.id == pelicula.id) {
            this.listado[i] = pelicula
          }
        })
      }else {
      pelicula.id = Panel.lastId + 1
      this.setPeliculas(pelicula)

      }
    }
    this.dibujarTemplate()
  }

  borrarPelicula(pelicula) { //poner en el boton borrar
      if (pelicula instanceof Pelicula) {
        this.listado.forEach((e, i) => {
          if (e.id == pelicula.id) {
            this.listado.splice(i, 1)
          }
        })
      }
    //se borra usando splice(index,1)
  }
}

class Pelicula {
  constructor(id, titulo, genero, anno) {
    this.id = id;
    this.titulo = titulo;
    this.genero = genero;
    this.anno = anno;
  }
}

var p = new Pelicula(1, "prueba", "genero", 1992)
var panel = new Panel()

window.onload= () => {
  panel.dibujarTemplate()
}
// panel.guardarBd(p)


console.log(panel.listado)
// panel.borrarPelicula(p)
console.log(panel.listado)

function hacer(event) {
  var idBtn = event.target.id
  var soy = idBtn.slice(0,4)
  console.log(soy)

}

btnGuardar.addEventListener('click', () => {
  var p = new Pelicula("",nombrePeli.value.trim(),generoPeli.value.trim(),annoPeli.value.trim())
  panel.guardarBd(p)
})
