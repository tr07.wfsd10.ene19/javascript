//Array para tomar los valores de los productos de la tienda
var cosas = ["Reloj de arena decorativo	TILLSYN,descripcion del objeto,https://www.ikea.com/es/es/images/range-introduction/ikea-ikea-tillsyn-hourglass-begåvning-glass-dome__1364493362974-s31.jpg,10",
  "LATTJO  Peluche,descripcion del objeto,https://www.ikea.com/es/es/images/products/lattjo-peluche-robot-verde-claro__0520262_pe642048_s4.jpg,12"
];
//Donde se cargaran los articulos a comprar
cesta = {};
/** Clase y constructor del objeto Ikea
 * @param  {} nombre nombre del producto
 * @param  {} desc descripcion del producto
 * @param  {} imagen imagen del producto
 * @param  {} precio precio del producto
 * @method {} comprar metodo para comprar un producto recibe @param cantidad, 
 *                    devuelve 0 en caso de que no haya disponibilidad y en caso positivo
 *                    devuelve la cantidad comprada
 * @returns {} Objeto instanciado de la clase Ikea
 */
var Ikea = function (nombre, desc, imagen, precio) {
  this.nombre = nombre;
  this.precio = precio;
  this.desc = desc;
  this.imagen = imagen;
  this.stock = 10;

  this.comprar = function (cantidad) {
    if (this.stock > 0) {
      this.stock--;
      return cantidad;
    }
    return 0;
  }
}

//Funcion que devuelve un array con todos los elementos del array cosas separados por comas (,)
var array = cosas.map(function (elem) {
  return elem.split(",");
});

//Se recorre el array con las cosas separadas por coma y se agregan al array arrayMuebles,
//pero los ingresa en forma de objetos
var arrayMuebles = [];
array.forEach(function (elem) {
  var mueble = new Ikea(elem[0].trim(), elem[1].trim(), elem[2].trim(), elem[3]);
  arrayMuebles.push(mueble);
});

//Selecciona los elementos del DOM donde se agregaran los productos
var template = document.querySelector("#item");
var newDiv = template.content.querySelector("div");


//Este FOR recorre el array arrayMuebles con objetos y los dibuja en el DOM
for (i = 0; i < arrayMuebles.length; i++) {
  var a = newDiv.cloneNode(true);
  a.id = "elemDiv" + i;

  a.querySelector("#nombre").innerHTML = arrayMuebles[i].nombre;
  a.querySelector("img").src = arrayMuebles[i].imagen;
  a.querySelector(".precio").innerHTML = arrayMuebles[i].precio + " €";

  var btn = a.querySelector("button");
  btn.id = i;
  btn.addEventListener("click", compra);
  document.querySelector("#colItems").appendChild(a);
}
/**Funcion para comprar toma el id del boton asignado a cada producto para saber cual se esta comprando
 * @param  {} event evento del mouse click que llama al metodo comprar del articulo
 */
function compra(event) {
  var idBtn = event.target.id;
  var articulo = arrayMuebles[idBtn];

  var divElem = document.querySelector("#elemDiv" + idBtn);
  var cantidad = 1;
  var res = arrayMuebles[idBtn].comprar(cantidad); //Se llama al metodo comprar del articulo en el array arrayMuebles cuyo ID sea igual al del boton clickeado

  if (res > 0) { //Dependiendo de la respuesta devuelta por el metodo comprar del articulo se decide que mensaje mostar
    msg = "Se ha(n) añadido " + res + " artículos a su cesta de la compra.";
    añadirACesta(articulo.nombre, cantidad);
  } else {
    msg = "No disponemos de suficiente stock";
  }

  divElem.querySelector(".stock").innerHTML = msg;

  setTimeout(function () {
    divElem.querySelector(".stock").innerHTML = "";
  }, 2000);


}

/**Funcion que agrega al objeto cesta los articulos comprados
 * @param  {} clave se refiere al nombre del articulo, se vuelve la clave
 * @param  {} cantidad la cantidad que se sumara a los articulos comprados, se vuelve el valor
 */
function añadirACesta(clave, cantidad) {
  cesta[clave] = cesta[clave] ? cesta[clave] + cantidad : 1;
  refrescarCesta();
  setTimeout(function () {
    document.querySelector("#cesta").classList.add("invisible");
  }, 10000);
}

/**Funcion que devuelve el nombre del articulo para que en la funcion refrescarCesta coloque el nombre del articulo
 * @param  {} texto string para comparar el nombre del articulo con los agregados en el array arrayMuebles
 * @returns {} Objeto con todas las propiedades
 */
function getElementoByNombre(texto) {
  for (var i = 0; i < arrayMuebles.length; i++) {
    if (arrayMuebles[i].nombre == texto) return arrayMuebles[i];
  }
}
/**Funcion para actualizar la lista de compra, el objeto cesta
 */
function refrescarCesta() {
  var template = document.querySelector("#itemsCesta");
  var divItemsCesta = template.content.querySelector("div");
  var divCesta = document.querySelector("#cesta");
  divCesta.classList.remove("invisible");
  divCesta.innerHTML = "";
  var costeCesta = 0;

  for (elemento in cesta) {
    var mueble = getElementoByNombre(elemento);
    var clonDiv = divItemsCesta.cloneNode(true);
    var cantidades = cesta[elemento];
    clonDiv.querySelector(".nombre").innerHTML = mueble.nombre;
    clonDiv.querySelector(".cantidad").innerHTML = cantidades;
    clonDiv.querySelector(".precio").innerHTML = mueble.precio + "€";
    divCesta.appendChild(clonDiv);
    costeCesta += cantidades * mueble.precio;
  }

  //Muestra el coste total de todos los productos de la cesta
  var total = document.createElement("div");
  total.innerHTML = "<strong>TOTAL DE SU COMPRA: " + costeCesta + " €</strong>";
  divCesta.appendChild(total);

}