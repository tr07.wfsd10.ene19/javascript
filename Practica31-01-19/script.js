class Heroe {
  constructor(name,faction,race,life){
    this._name = name
    this._race = race
    this._faction = faction
    this._life = life
  }
  static Clases () {
    var _clases = ["Warrior","Paladin","Mage","Warlock","Druid","Shaman","Priest","Hunter","Rogue","Death Knigth","Demon Hunter","Monk"] 
    return _clases
  }
  static Razas() {
    var _razas = ["Human","Dwarf","Gnome","Worgen","Night Elf","Draenei","Orc","Tauren","Undead","Blood Elf","Troll","Goblin","Pandaren"]
  }
  static Profesion(){
    var profesiones = ["Alchemy","Blacksmithing","Cooking","Enchanting"]
    return profesiones
  }
  static factions(){
      var _factions = ["ALLIANCE", "HORDE"]
      return _factions
    }
    saludar(){
      var say = this._faction == "ALLIANCE" ?  "FOR THE ALLIANCE!!!": "FOR THE HORDE!!!"
      return say
    }
    
}

class Clase extends Heroe{
  constructor(_name,_faction, _race, _life, _profession, _habilidades){
    super(_name,_faction,_race,_life)
    this._profession =_profession
    this._habilidades = _habilidades
  }
  atacar(personaje){
    var golpe = Math.floor(Math.random() * 10) + 1
    personaje._life = personaje._life - golpe
    return "Toma esto!! "+personaje._name+" - Golpeado por: "+golpe
  }

  trabajar(){
    return "I'll do some "+this._profession
  }

  set profesion(valor){
    if(typeof valor == 'string'){
      this._profession = valor
    }
  }
}
// pruebas
var tanque = new Clase('Arthas','ALLIANCE','Human','20','Enchanting','Death Knigth')
console.log(tanque)
tanque.profesion = 'Blacksmithing'
console.log(tanque)
console.log(tanque.saludar())
console.log(tanque.trabajar())
var spell = new Clase('Thrall','HORDE','Orc','20','Herbalism','Shaman')
console.log(spell)
spell.profesion = 'Engineering'
console.log(spell)
console.log(spell.saludar())
console.log(spell.trabajar())
console.log("PELEA!!!")
tanque.atacar(spell)
console.log(spell)
// pruebas

// lectura del DOM

//alianza creacion
var nameA = document.querySelector("#nameA")
var raceA = document.querySelector("#raceA")
var classA = document.querySelector("#classA")
var btnCrearA = document.querySelector("#btnA")

//alianza mostrar y atacar
var nombreA = document.querySelector("#nombreA")
var vidaA = document.querySelector("#vidaA")
var razaA = document.querySelector("#razaA")
var imgA = document.querySelector("#imgA")
var btnAtacarA = document.querySelector("#btnAtacarA")

//horda creacion
var nameH = document.querySelector("#nameH")
var raceH = document.querySelector("#raceH")
var classH = document.querySelector("#classH")
var btnCrearH = document.querySelector("#btnH")

//alianza mostrar y atacar
var nombreH = document.querySelector("#nombreH")
var vidaH = document.querySelector("#vidaH")
var razaH = document.querySelector("#razaH")
var imgH = document.querySelector("#imgH")
var btnAtacarH = document.querySelector("#btnAtacarH")

//funciones

//funcion para cambiar imagen segun raza seleccionada
var showImg = (seleccion) => {
  var valor = 0
  if(seleccion){
  switch (seleccion) {
    case "human": 
      valor =  "img/human.gif"
      break
    case "dwarf":
      valor = "img/dwarf.gif"
      break
    case "nightelf":
      valor = "img/nightelf.gif"
      break
    case "gnome":
      valor = "img/gnome.gif"
      break
    case "orc":
      valor = "img/orc.gif"
      break
    case "tauren":
      valor = "img/tauren.gif"
      break
    case "troll":
      valor = "img/troll.gif"
      break
    case "undead":
      valor = "img/undead.gif"
      break
  }
  return valor
}
}
var asignar = (objeto) => {
  if (objeto._faction == "ALIANZA") {
  nombreA.innerHTML = objeto._name
  vidaA.innerHTML = objeto._life
  razaA.innerHTML = objeto._race
  claseA.innerHTML = objeto._habilidades
}else {
  nombreH.innerHTML = objeto._name
  vidaH.innerHTML = objeto._life
  razaH.innerHTML = objeto._race
  claseH.innerHTML = objeto._habilidades
}
}
var alianza = {}
var horda = {}

var crear = (nombre,faccion,raza,clase,img) => {
  if (faccion == "ALIANZA") {
    alianza =  new Clase(nombre,faccion,raza,20,"Herrero",clase)
    imgA.src = img
  }else {
    horda =  new Clase(nombre,faccion,raza,20,"Minero",clase)
    imgH.src = img
  }
}

btnCrearA.addEventListener('click',() =>{
crear(nameA.value.trim(),"ALIANZA",raceA.value,classA.value,showImg(raceA.value)),
asignar(alianza)
})

btnCrearH.addEventListener('click', () => {
  crear(nameH.value.trim(),"HORDA",raceH.value,classH.value,showImg(raceH.value)),
  asignar(horda)
})

var dibujar = (text) => {
  var scroll = document.querySelector("#battleLog")
  var nuevoLi = document.createElement("LI")
  var dialogo = document.createTextNode(text)
  nuevoLi.appendChild(dialogo)
  nuevoLi.classList.add("list-group-item")
  scroll.appendChild(nuevoLi)
  scroll.scrollTo = scroll.scrollHeight
}

btnAtacarA.addEventListener('click',() => {
  dibujar(alianza.atacar(horda))
  vidaH.innerHTML = horda._life
  if(horda._life <= 0) {
    btnAtacarA.disabled = true
    btnAtacarH.disabled = true
    dibujar(horda._name+" ha muerto")
  }
  btnAtacarA.disabled = true
  setTimeout(() => {
    btnAtacarA.disabled = false
  }, 2000)
})

btnAtacarH.addEventListener('click', () => {
  dibujar(horda.atacar(alianza))
  vidaA.innerHTML = alianza._life
  if(alianza._life <= 0) {
    btnAtacarH.disabled = true
    btnAtacarA.disabled = true
    dibujar(alianza._name+" ha muerto")
  }
  btnAtacarH.disabled = true
  setTimeout(() => {
    btnAtacarH.disabled = false
  }, 2000)
})